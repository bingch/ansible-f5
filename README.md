Role Name
=========

Setup F5 irule/policy/pool/virtual server

Requirements
------------

f5-sdk >= 3.0.16

Role Variables
--------------

See example

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------
<pre>
- name: manage f5 related stuff like vs, pools etc
  hosts: f5_pool_host
  gather_facts: no
  connection: local

  vars:
    desc: "managed by ansible"
    f5_cred:
      password: secret
      server: f5_server
      user: ansible_user
      validate_certs: true
      server_port: 443
    f5_irule:
      - name: my_irule
        src: '{{ inventory_dir }}/files/f5/my_irule.tcl'
        partition: mypart
    f5_asm_policy:
      - name: my_asm_policy
        template: Fundamental
        partition: mypart
    f5_policy:
      - name: my_policy
        description: '{{ desc }}'
        partition: mypartition
        rules:
          - name: default
            conditions:
              - type: all_traffic
            actions:
              - type: enable
            asm_policy: '/mypart/my_asm_policy'
    f5_pool:
      - name: my_pool
        lb_method: round-robin
        description: '{{ desc }}'
        partition: mypart
        monitors:
          - '/Common/http'
        member:
          - name: node1
            host: 1.2.3.4
            port: 80
    f5_vs:
      # https front end
      - name: my_virtual_server 
        description: '{{ desc }}'
        partition: mypart
        irules:
          - '/mypart/my_irule'
        destination: 4.3.2.1
        port: 443
        pool: my_pool
        snat: Automap
        default_persistence_profile: /Common/cookie             

  roes:
    - f5
</pre>
License
-------

GPL v3

Author Information
------------------

bing https://gitlab.com/bingch
